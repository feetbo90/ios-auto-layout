//
//  DetailViewController.swift
//  BelajarViewDicoding
//
//  Created by Muhammad Iqbal Pradipta on 20/05/21.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var textTitle: UILabel!
    @IBOutlet weak var gambarImageView: UIImageView!
    @IBOutlet weak var textDescription: UILabel!
    // Digunakan untuk menampung data sementara
    var detail: (title: String?, desc: String?, image: UIImage?)
        
    override func viewDidLoad() {
        super.viewDidLoad()
            
            // Digunakan untuk mengubah konten setiap kali membuka halaman detail
        textTitle.text = detail.title
        textDescription.text = detail.desc
        gambarImageView.image = detail.image
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
